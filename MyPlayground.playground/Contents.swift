import UIKit

var data = [1.5, 30, 6, 1.5]

let processInfo = ProcessInfo.processInfo
let beginTime = processInfo.systemUptime


//MARK: - Functions

//Проверяем начальные данные. По условиям задания необходимо ограничить размер входящего массива для разумного выполнения. Количество начальных элементов зависит от платформы исполнения алгоритма.
func isCorrectData(data: [Double]) -> Bool {
  //Сложность этой проферки - О(n). Необходима она или нет - зависит от более точной проработки задачи.
  for number in data {
    if number <= 0 {
      return false
    }
  }
  
  return data.count < 2000 //При количестве 2000 начальных данных, алгоритм выполнится за 3-4 секунды (4GHz i7)
}

//Сложность этой функции - O(n), так как проходимся 1 раз по элементам массива. В памяти, помимо исходного массива, резервируем место для одной переменной
func calculateSumm(from data: [Double]) -> Double {
  var summ = 0.0
  
  for number in data {
    summ += number
  }
  
  return summ
}

//Сложность этой функции - O(n), так как проходимся 1 раз по элементам массива. В памяти, помимо исходного массива, резервируем место для одной константы - для суммы. Для экономии памяти будем заменять элементы входящего массива на преобразованные к долям.
func convertToProportion(from numbers: [Double]) -> [Double] {
  var numbers = numbers
  let summ = calculateSumm(from: data)
  
  for (index, number) in numbers.enumerated() {
    numbers[index] = number / summ * 100
  }
  
  return numbers
}

//Сложность этой функции - O(n), так как проходимся 1 раз по элементам массива. В памяти, помимо исходного массива, резервируем места для массива с отформатированными числами.
func formate(numbers: [Double]) -> [String] {
  var formattedNumbers = [String]()
  
  for number in numbers {
    formattedNumbers += [String(format: "%0.3f", number)]
  }
  
  return formattedNumbers
}


//MARK: - Main

if isCorrectData(data: data) {
  let proportionNumbers = convertToProportion(from: data)
  let formattedNumbers = formate(numbers: proportionNumbers)
  
  print(formattedNumbers)
  print(processInfo.systemUptime - beginTime)  //время выполнения
} else {
  print("Некорректные начальные данные")
}

/*
 Комментарии:
 1) Сложность задачи по шкале от 1 до 10 - 1, так как задача предельно простая.
 2) Предварительная оценка трудозатрат - 1 час.
 3) Фактические трудозатраты. 10 минут за код в базовом исполнении + 10 минут на комментарии к коду
 */

